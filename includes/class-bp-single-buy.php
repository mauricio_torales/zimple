<?php


class ZimpleSingleBuy{
  /*
  * funcion para genera el procces para realizar el pago de la transaccion
    $v_amount decimal monto de la transacion
    $v_descripcion varchar descripcion dl pago
    $v_shop_process_id varchar numero identificardor enviado a bancard
  */
  public function zimpleGenerarProccess($v_amount,$v_description,$v_shop_process_id, $publicKey, $privatecKey, $urlCancel, $urlSuccess,$host,$numeroZimple){
    $url = $host."/vpos/api/0.3/single_buy";
    $v_amount = number_format($v_amount,2,'.','');
    $v_currency = "PYG";
    $v_additional_data = "";
    $v_token = $privatecKey . $v_shop_process_id . $v_amount . $v_currency;
    $v_token = md5($v_token);
    $data = json_encode(array(
      "public_key" => $publicKey,
      "operation"  => array(
        "token" => "$v_token",
        "shop_process_id" => "$v_shop_process_id",
        "amount" => "$v_amount",
        "currency" => "$v_currency",
        'additional_data' => "$numeroZimple",
        "description" => "$v_description",
        "return_url" => "$urlSuccess" . $v_shop_process_id,
        "cancel_url" => "$urlCancel" . $v_shop_process_id,
        "zimple"=> "S"
      ),
    ));

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_USERAGENT, $defined_vars['HTTP_USER_AGENT']); 
    $result = curl_exec($ch);
    $result = json_decode($result, false);
    $result = json_decode(json_encode($result), true);
    if($result['status'] == "success"){
      $urlRetorno =  $result['process_id'];
    }
    curl_close($ch);
    return $urlRetorno;
  }

  public function zimpleRoolBack($v_shop_process_id,$host, $publicKey, $privatecKey){
    $url = $host."/vpos/api/0.3/single_buy/rollback";
    $v_token = md5( $privatecKey . $v_shop_process_id . "rollback" . "0.00"  );
    $data = json_encode(array(
      "public_key" => $publicKey,
      "operation"  => array(
        "token" => "$v_token",
        "shop_process_id" => "$v_shop_process_id"
      ),
    ));
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    $result = json_decode($result, false);
    $result = json_decode(json_encode($result), true);
    curl_close($ch);
    return $result;
  }
}
?>