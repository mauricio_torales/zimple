<?php
  
    add_action('plugins_loaded', 'init_zimple_gateway_class');
    require_once plugin_dir_path(__FILE__) . 'class-bp-single-buy.php';
    require_once plugin_dir_path(__FILE__) . 'class-wc-admin-duplicate-order.php';


    function init_zimple_gateway_class(){
        class WC_Zimple_Custom extends WC_Payment_Gateway {
            public $domain;
            public static $log_enabled = false;

            /**
             * Constructor for the gateway.
             */
            public function __construct() {

                $this->domain = 'zimple_payment';
                $this->id                 = 'zimple';
                $this->icon               = apply_filters('woocommerce_zimple_gateway_icon', plugins_url( 'img/zimple.png', __FILE__ ));
                $this->has_fields         = false;
                $this->method_title       = __( 'Zimple', $this->domain );
                $this->method_description = __( 'Permite pagos con la billetera Zimple.', $this->domain );

                // Load the settings.
                $this->init_form_fields();
                $this->init_settings();

                // Define user set variables
                $this->title        = $this->get_option( 'title' );
                $this->description  = $this->get_option( 'description' );
                $this->instructions = $this->get_option( 'instructions', $this->description );
                $this->order_status = $this->get_option( 'order_status', 'completed' );
                $this->order_button_text = __( 'Ir al pago', $this->domain );
                $this->debug          = 'yes' === $this->get_option( 'debug', 'no' );
                $this->email          = $this->get_option( 'email' );
                $this->receiver_email = $this->get_option( 'receiver_email', $this->email );
                self::$log_enabled    = $this->debug;

                // Actions
                add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
                add_action( 'woocommerce_thankyou_custom', array( $this, 'thankyou_page' ) );

                // Customer Emails
                add_action( 'woocommerce_email_before_order_table', array( $this, 'email_instructions' ), 10, 3 );
            }


            public static function log( $message, $level = 'info' ) {
                if ( self::$log_enabled ) {
                    if ( empty( self::$log ) ) {
                        self::$log = wc_get_logger();
                    }
                    self::$log->log( $level, $message, array( 'source' => 'zimple' ) );
                }
            }

            /**
             * Initialise Gateway Settings Form Fields.
             */
            public function init_form_fields() {

                $this->form_fields = array(
                    'enabled' => array(
                        'title'   => __( 'Enable/Disable', $this->domain ),
                        'type'    => 'checkbox',
                        'label'   => __( 'Habilitar Pago con Zimple', $this->domain ),
                        'default' => 'yes'
                    ),
                    'title' => array(
                        'title'       => __( 'Titulo', $this->domain ),
                        'type'        => 'text',
                        'description' => __( 'This controls the title which the user sees during checkout.', $this->domain ),
                        'default'     => __( 'Zimple', $this->domain ),
                        'desc_tip'    => true,
                    ),
                    'comercio' => array(
                        'title'       => __( 'Comercio', $this->domain ),
                        'type'        => 'text',
                        'description' => __( 'Nombre de comercio para mostrar en Zimple.', $this->domain ),
                        'default'     => __( 'Shop', $this->domain ),
                        'desc_tip'    => true,
                    ),
                    'order_status' => array(
                        'title'       => __( 'Estado de Orden', $this->domain ),
                        'type'        => 'select',
                        'class'       => 'wc-enhanced-select',
                        'description' => __( 'Estado del pedido luego de ser procesado.', $this->domain ),
                        'default'     => 'wc-completed',
                        'desc_tip'    => true,
                        'options'     => wc_get_order_statuses()
                    ),
                    'PublicKey' => array(
                        'title'       => __( 'Clave Publica', $this->domain ),
                        'type'        => 'text',
                        'description' => __( 'Clave Publica de zimple.', $this->domain ),
                        'default'     => 'Clave Publica de zimple.',
                        'desc_tip'    => true,
                    ),
                    'PrivateKey' => array(
                        'title'       => __( 'Clave Privada', $this->domain ),
                        'type'        => 'text',
                        'description' => __( 'Clave Privada de zimple.', $this->domain ),
                        'default'     => 'Clave Privada de zimple.',
                        'desc_tip'    => true,
                    ),
                    'CancelUrl' => array(
                        'title'       => __( 'Url de Cancelacion', $this->domain ),
                        'type'        => 'text',
                        'description' => __( 'Url si el usuario cancela el pedido, osea no paga.', $this->domain ),
                        'default'     => '/finalizar-compra/cancel-zimple/',
                        'desc_tip'    => true,
                    ),
                    'SuccessUrl' => array(
                        'title'       => __( 'Url de Exito', $this->domain ),
                        'type'        => 'text',
                        'description' => __( 'Url si el usuario paga con exito el pedido.', $this->domain ),
                        'default'     => '/finalizar-compra/success-zimple/',
                        'desc_tip'    => true,
                    ),
                    'UrlEntorno' => array(
                        'title'       => __( 'Entorno de Zimple', $this->domain ),
                        'type'        => 'text',
                        'description' => __( 'Host de api, desarrollo/produccio.', $this->domain ),
                        'default'     => 'Host de api, desarrollo/produccio.',
                        'desc_tip'    => true,
                    ),
                    'description' => array(
                        'title'       => __( 'Descripcion', $this->domain ),
                        'type'        => 'textarea',
                        'description' => __( 'Pago con billetera zimple.', $this->domain ),
                        'default'     => __('Pago con billetera zimple.', $this->domain),
                        'desc_tip'    => true,
                    ),
                    'instructions' => array(
                        'title'       => __( 'Instruccion', $this->domain ),
                        'type'        => 'textarea',
                        'description' => __( 'Método de pago la billetera zimple.', $this->domain ),
                        'default'     => '',
                        'desc_tip'    => true,
                    )
                );
            }

            /**
             * Output for the order received page.
             */
            public function thankyou_page() {
                if ( $this->instructions )
                    echo wpautop( wptexturize( $this->instructions ) );
            }

            /**
             * Add content to the WC emails.
             *
             * @access public
             * @param WC_Order $order
             * @param bool $sent_to_admin
             * @param bool $plain_text
             */
            public function email_instructions( $order, $sent_to_admin, $plain_text = false ) {
                if ( $this->instructions && ! $sent_to_admin && 'zimple' === $order->get_payment_method() && $order->has_status( 'on-hold' ) ) {
                    echo wpautop( wptexturize( $this->instructions ) ) . PHP_EOL;
                }
            }

            /**
             * Process the payment and return the result.
             *
             * @param int $order_id
             * @return array
             */
            public function process_payment( $order_id ) {
                WC()->session->set('orderduplicate' , 0);
                $publicKey = $this->get_option('PublicKey');
                $privatecKey = $this->get_option('PrivateKey');
                $urlCancel = site_url() . $this->get_option('CancelUrl');
                $urlSuccess = site_url() . $this->get_option('SuccessUrl');
                $host = $this->get_option('UrlEntorno');
                $comercio = $this->get_option('comercio');
                $zimple = New ZimpleSingleBuy();
                if(empty($_POST['numero_zimple'])){
                    wc_add_notice(  'Por favor cargue el número de la billetera.', 'error' );
                    return;
                }
                $order = wc_get_order($order_id);
                $shop_proccess = 0;
                $shop_proccess = $zimple->zimpleGenerarProccess($order->get_total(),'Compra desde '. $comercio ,$order->get_id(), $publicKey, $privatecKey, $urlCancel, $urlSuccess, $host,$_POST['numero_zimple']);
                
               if($shop_proccess==null){
                    wc_add_notice(  'Hubo un error, vuelva a intentarlo, pruebe vaciando su carrito y cargue de nuevo su producto.', 'error' );
                    return;
               }
                WC()->session->set('shop_process_id' , $shop_proccess);
                WC()->session->set('host' , site_url());
                WC()->session->set('host_zimple' , $host);
                return array(
                    'result'    => 'success',
                    'redirect'  => $this->get_return_url( $order )
                );
            }

        
            public function cancelOrder($orderID){
                $publicKey = $this->get_option('PublicKey');
                $privatecKey = $this->get_option('PrivateKey');
                $host = $this->get_option('UrlEntorno');
                $zimple = New ZimpleSingleBuy();
                $resquest = $zimple->zimpleRoolBack($orderID, $host, $publicKey, $privatecKey);
                return $resquest;
            }
        }
    }

    function my_scripts_zimple() {
        wp_enqueue_script('zimple', plugin_dir_url(__FILE__) . 'js/zimple.js', true); 
    }
    add_action( 'enqueue_scripts', 'my_scripts_zimple' );



    add_filter( 'rewrite_rules_array','zimple_rewrite_rules' );
    add_filter( 'query_vars','zimple_query_vars' );
    add_action( 'wp_loaded','zimple_rules' );
    // creacion de nueva ruta para zimple
    function zimple_rules(){
        $rules = get_option( 'rewrite_rules' );
        if ( ! isset( $rules['finalizar-compra/zimple-payment/([^/]+)/([^/]+)/?'] ) || isset( $rules['finalizar-compra/cancel-zimple/([^/]+)/?'] ) || isset( $rules['finalizar-compra/success-zimple/([^/]+)/?'] ) ) {
            global $wp_rewrite;
            $wp_rewrite->flush_rules();
        }
    }

    // Adding a new rule
    function zimple_rewrite_rules( $rules ){
        $newrules = array();
        $newrules['finalizar-compra/zimple-payment/([^/]+)/([^/]+)/?'] = 'index.php?order=$matches[1]&key=$matches[2]';
        $newrules['finalizar-compra/cancel-zimple/([^/]+)/?'] = 'index.php?order=$matches[1]';
        $newrules['finalizar-compra/success-zimple/([^/]+)/?'] = 'index.php?order=$matches[1]';
        return $newrules + $rules;
    }

    // Adding the id var so that WP recognizes it
    function zimple_query_vars( $vars ){
        array_push($vars, 'myroute', 'myargument');
        return $vars;
    }


    add_filter( 'template_include', function ( $template ) {
        global $wp;
        $urlParciales = explode('/', $wp->request);
        if(!isset($urlParciales[1])){
            $urlParciales[1] = '';
        }
        $zimple_template = plugin_dir_path(__FILE__) . 'templates/checkout_zimple.php';
        $zimple_template_cancel = plugin_dir_path(__FILE__) . 'templates/cancel_zimple.php';
        $zimple_template_success = plugin_dir_path(__FILE__) . 'templates/success_zimple.php';

        if ( $urlParciales[0]=='finalizar-compra' && $urlParciales[1]=='zimple-payment' && $zimple_template ) {
            return $zimple_template;
        }

        if ( $urlParciales[0]=='finalizar-compra' && $urlParciales[1]=='cancel-zimple' && $zimple_template_cancel ) {
            return $zimple_template_cancel;
        }

        if ( $urlParciales[0]=='finalizar-compra' && $urlParciales[1]=='success-zimple' && $zimple_template_success ) {
            return $zimple_template_success;
        }
      
       
        return $template;
    });

    // pagina de redireccion para zimple
    add_action( 'template_redirect', 'zimple_payment_redirect');
    function zimple_payment_redirect(){
        if ( is_wc_endpoint_url( 'order-received' ) ) {
            global $wp;
            
            // Get the order ID
            $order_id =  intval( str_replace('finalizar-compra/order-received/', ' ', $wp->request ) );
            // Get an instance of the WC_Order object
            $order = wc_get_order( $order_id );
           // $method = get_post_meta( $order->get_id(), '_payment_method', true );
           $method = $order->get_payment_method();

            // Set HERE your Payment Gateway ID  
            
            if($method == 'zimple' ){
                
                // Set HERE your custom URL path
                wp_redirect(home_url('finalizar-compra/zimple-payment/' . $order_id . '/' . WC()->session->get('shop_process_id') . '/'));
                exit(); // always exit
            }
           
        }
    }

    add_action( 'woocommerce_order_status_cancelled', 'zimple_status_to_refund',21, 1 );
    function zimple_status_to_refund( $order_id ) {
    $order = new WC_Order( $order_id );
        if($order->get_payment_method() == 'zimple' ){
             $classWP = new WC_Zimple_Custom();
             $request = $classWP->cancelOrder($order_id);
            if ($request['status']=='success') {
                 $refund_amount = ($refund_amount * $refundPercentage);
                 $refund_reason = "Orden Cancelada";
                 $refund = wc_create_refund( array(
                   'amount'         => $refund_amount,
                   'reason'         => $refund_reason,
                   'order_id'       => $order_id,
                   'line_items'     => $line_items,
                   'refund_payment' => true
                   ));
                   $order->add_order_note( sprintf( __( 'Cancelacion Description %1$s' ), $request['messages'][0]['dsc']));
                   $order->update_status('wc-refunded', 'Order Cancelled And Completely
                    Refunded');
            }
        }
    }






    add_filter( 'page_template', 'mi_funcion_zimple' );
    function mi_funcion_zimple( $page_template ) {

     if ( is_page( 'accion' ) ) {
       $page_template = plugin_dir_path( __FILE__ ) . "/templates/checkout_zimple.php";
     }
     return $page_template;
    }

    add_filter( 'woocommerce_payment_gateways', 'add_zimple_gateway_class' );
    function add_zimple_gateway_class( $methods ) {
        $methods[] = 'WC_Zimple_Custom';
        return $methods;
    }

    add_action('woocommerce_checkout_process', 'process_zimple_payment');
    function process_zimple_payment(){
        if($_POST['payment_method'] != 'zimple')
            return;
    }


    /**
     * Update the order meta with field value
     */
    add_action( 'woocommerce_checkout_update_order_meta', 'zimple_payment_update_order_meta' );
    function zimple_payment_update_order_meta( $order_id ) {
        if($_POST['payment_method'] != 'zimple')
            return;
    }

    /**
     * Display field value on the order edit page
     */
    add_action( 'woocommerce_admin_order_data_after_billing_address', 'zimple_checkout_field_display_admin_order_meta', 10, 1 );
    function zimple_checkout_field_display_admin_order_meta($order){
        //$method = get_post_meta( $order->get_id(), '_payment_method', true );
        $method = $order->get_payment_method();
        if($method != 'zimple')
            return;

    }

    add_filter( 'woocommerce_gateway_description', 'gateway_bacs_numero_zimple', 20, 2 );
    function gateway_bacs_numero_zimple( $description, $payment_id ){
        //
        if( 'zimple' === $payment_id ){
            ob_start(); // Start buffering

            echo '<div  class="bacs-fields" style="padding:10px 0;">';

            woocommerce_form_field( 'numero_zimple', array(
                'type'          => 'text',
                'label'         => __("Número de billetera zimple", "woocommerce"),
                'class'         => array('form-row-wide'),
                'required'      => true,
            ), '');

            echo '<div>';

            $description .= ob_get_clean(); // Append buffered content
        }
        return $description;
    }

?>