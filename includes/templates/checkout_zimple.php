<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

get_header();
$urlParciales = explode('/', $wp->request);
$order = wc_get_order( $urlParciales[2]);
$tarjetas = [];
$tarjetas = WC()->session->get('cards');
$host_zimple = WC()->session->get('host_zimple');

?>

<head>
	<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
	<?php if(strpos($host_zimple, "https://vpos.infonet.com.py:8888") !== false): ?>
		<script src="<?php echo plugins_url() . '/zimple/assets/js/bancard-checkout-3.0.0-sandbox.js' ;?>"></script>
	<?php else: ?>
		<script src="<?php echo plugins_url() . '/zimple/assets/js/bancard-checkout-3.0.0.js' ;?>"></script>
	<?php endif; ?>
</head>
 <script type="text/javascript">
 	$(document).ready(function() {
		
		var processID = '<?php echo $urlParciales[3]; ?>';
	    window.onload = function () {
			var styles = {
			
			};

	      	options = {
	        	styles: styles
	      	}
	      	if (processID!=0) {
	  			Bancard.Zimple.createForm('iframe-container', processID, options);
			}


		};
	});
	
 	
</script>
	<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received " style=" margin: auto;
    width: 50%;
    /* border: #f86823 solid; */
    padding: 10px;
    display: block;
    text-align: center;
    line-height: 150%;
    font-size: 1.85em;"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Pago con Billetera Zimple.', 'woocommerce' ), $order ); ?></p>

	<div style="height: 100px; width: 50%; padding-top: 0px; margin:auto; margin-bottom:300px" id="iframe-container">
	</div>

<div class="woocommerce-order">
	<?php

?>

	<?php if ( $order ) : ?>

		<?php if ( $order->has_status( 'failed' ) ) : ?>


		<?php else : ?>



		<?php endif; ?>

	<?php else : ?>


	<?php endif; ?>

</div>

<style>
li:last-child {
   list-style-type: none; 
}
</style>
<style>
.page-header {
   display: none;
}
</style>

<?php
get_footer();



