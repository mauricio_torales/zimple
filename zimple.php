<?php 
/*
Plugin Name: Billetera Zimple Woocommerce
Description: Plugin para pagos con la Billetera de Zimple
Author: Mauricio Torales
Version: 1.0.0
*/

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}


/**
 * Bancard Payment Gateway.
 *
 * Provides a Bancard Payment Gateway, mainly for testing purposes.
 */

    require_once plugin_dir_path(__FILE__) . 'includes/zimple-functions.php';

 ?>